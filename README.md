# SideBar

This project mean to offer user a cli to create sidebar for
[vuepress v2.x](https://v2.vuepress.vuejs.org/) and
[vitepress](https://vitepress.vuejs.org/). For
[vuepress v1.x](https://vuepress.vuejs.org/), try
[vuepress-plugin-beautiful-bar](https://www.npmjs.com/package/vuepress-plugin-beautiful-bar)

## Usage

```shell
yarn add -D @minghuhugo/sidebar
npx vsidebar
```

For more specific usage, try `vsidebar -h`
