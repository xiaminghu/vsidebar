# SideBar

First of all, we need to figure out the differences between
[vuepress-next](https://github.com/vuepress/vuepress-next) and
[vitepress](https://github.com/vuejs/vitepress).

> vuepress-next

```shell
// .vuepress/config.ts
module.exports = {
  themeConfig: {
    navbar: [
      { text: "", link: "" },
      {
        text: "NodeJS",
        // vuepress use children
        children: [
          '/nodejs/vue/README.md'
          { text: "Vue", link: "/nodejs/vue/" }
        ]
      }
    ],
    sidebar: {
      "/nodejs/vue/": [
        // refer to /nodejs/vue/README.md
        "/nodejs/vue/",
        {
          text: "Vuepress",
          children: [
            "/nodejs/vue/vuepress/README.md",
            "/nodejs/vue/vuepress/Introduction.md"
          ] 
        }
      ]
    }
  }
}
```

> vitepress

```shell
// .vitepress/config.ts
module.exports = {
  themeConfig: {
    nav: [
      { text: "", link: "" },
      {
        text: "NodeJS",
        // vitepress use items
        items: [
          { text: "Vue", link: "/nodejs/vue/" }
        ]
      }
    ],
    sidebar: {
      "/nodejs/vue/": [
        // refer to /nodejs/vue/index.md
        { text: "Vue", link: "/nodejs/vue/" },
        {
          text: "Vuepress",
          children: [
            { text: "README", link: "/nodejs/vue/vuepress/" },
            { text: "Introduction", link: "/nodejs/vue/vuepress/Introduction" }
          ] 
        }
      ]
    }
  }
}
```

> now let's compare the difference between above two config:

- where config.ts lies:
  - vuepress use `.vuepress/config.ts`
  - vitepress use `.vitepress/config.ts`
- key of navbar:
  - vuepress use `navbar`
  - vitepress use `nav`
- children or items:
  - vuepress use `children` for navItem
  - vitepress use `items` for navItem
- default file to read:
  - vuepress defaults to read `README.md`
  - vitepress default to read `index.md`
- remove or remains suffix
  - vuepress remains suffix
  - vitepress removes suffix
- navbarItemAsString
  - vuepress accepts string as a navbarItem
  - vitepress doesn't
- sidebar collapsable
  - vuepress takes effect
  - vitepress doesn't currently, but seems to support in future
- sidebarLink
  - vuepress use string for path resolve
  - vitepress use SideBarLink Object for path resolve
