import nav from "./nav.json"
import sidebar from "./sidebar.json"

module.exports = {
  themeConfig: {
    nav: nav,
    sidebar: sidebar
  }
}
