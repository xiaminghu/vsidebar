import navbar from "./nav.json"
import sidebar from "./sidebar.json"

module.exports = {
  bundler: '@vuepress/bundler-vite',
  themeConfig: {
    navbar: navbar,
    sidebar: sidebar
  }
}
