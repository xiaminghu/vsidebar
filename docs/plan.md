# Plan

## V0.0.1

- √ support vuepress and vitepress sidebar generation
- √ basic documentation
- test cases
- √ user-friendly usage
  - √ provide cli
  - √ simple usage (auto detection)
