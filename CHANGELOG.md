## 0.0.1 (2021-11-05)


### Bug Fixes

* ensure forward slash before every relative path ([c56bc48](https://gitee.com/xiaminghu/vsidebar/commits/c56bc480bae2a9d1bb5b38b15d69ae129ac9be38))


### Features

* determine params by auto detection ([e4b6fd1](https://gitee.com/xiaminghu/vsidebar/commits/e4b6fd13a57d890c341b7167ca12201ae1d0388d))
* generate vitepress sidebar correctly ([a23b909](https://gitee.com/xiaminghu/vsidebar/commits/a23b909067ec844c1e12c777797729f48a9d8138))
* serve docs with vite and vue correctly ([cb10b95](https://gitee.com/xiaminghu/vsidebar/commits/cb10b9543b80072231242bbd0085d67dd659bde9))
* support cli ([2959dbd](https://gitee.com/xiaminghu/vsidebar/commits/2959dbda8d86fcc957a01448946d07eadf962eae))
* support generate vuepress sidebar ([69fa139](https://gitee.com/xiaminghu/vsidebar/commits/69fa1395db219c109c0a2b4b2fef44836d099d08))



