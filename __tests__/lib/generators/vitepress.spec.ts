import { Config } from 'src/types/index'
import { VitepressGenerator } from 'src/lib/generators/vitepress'

import * as path from 'path'

const PROJECT_DIR = process.cwd()
const DOCS_DIR = path.join(PROJECT_DIR, 'docs', 'vite-docs')
const CONFIG_DIR = path.join(DOCS_DIR, '.vitepress')
const NAVBAR = path.join(CONFIG_DIR, 'nav.json')
const SIDEBAR = path.join(CONFIG_DIR, 'sidebar.json')

describe('lib/generators/vitepress', () => {
  let config: Config = {
    generator: 'vitepress',
    docsDir: DOCS_DIR,
    navbar: NAVBAR,
    output: SIDEBAR,
  }
  let generator = new VitepressGenerator(config)
  let navbar = generator.loadNavBar()
  let links = generator.navToRelLinks(navbar)
  let data = generator.createSideBarConfig(links)
  describe('testLoadNavBar', () => {
    it('navbar should be correct', () => {
      expect(navbar).toStrictEqual([
        { text: '', link: '' },
        {
          text: 'NodeJS',
          items: [{ text: 'Vue', link: '/nodejs/vue/' }],
        },
      ])
    })
  })
  describe('testNavToRelLinks', () => {
    it('link should be correct', () => {
      expect(links).toStrictEqual(['/nodejs/vue/'])
    })
  })
  describe('testCreateSideBarConfig', () => {
    it('sidebar should be correct ', () => {
      expect(data).toStrictEqual({
        '/nodejs/vue/': [
          { text: 'Index', link: '/nodejs/vue/' },
          {
            text: 'Vuepress',
            children: [
              { text: 'Introduction', link: '/nodejs/vue/vuepress/Introduction' },
              { text: 'Index', link: '/nodejs/vue/vuepress/' },
            ],
          },
        ],
      })
    })
  })
})
