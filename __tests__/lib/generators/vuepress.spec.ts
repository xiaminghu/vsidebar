import { Config } from 'src/types/index'
import { VuepressGenerator } from 'src/lib/generators/vuepress'

import * as path from 'path'

const PROJECT_DIR = process.cwd()
const DOCS_DIR = path.join(PROJECT_DIR, 'docs', 'vue-docs')
const CONFIG_DIR = path.join(DOCS_DIR, '.vuepress')
const NAVBAR = path.join(CONFIG_DIR, 'nav.json')
const SIDEBAR = path.join(CONFIG_DIR, 'sidebar.json')

describe('lib/generators/vuepress', () => {
  let config: Config = {
    generator: 'vuepress',
    docsDir: DOCS_DIR,
    navbar: NAVBAR,
    output: SIDEBAR,
  }
  let generator = new VuepressGenerator(config)
  let navbar = generator.loadNavBar()
  let links = generator.navToRelLinks(navbar)
  let data = generator.createSideBarConfig(links)
  describe('testLoadNavBar', () => {
    it('navbar should be correct', () => {
      expect(navbar).toStrictEqual([
        { text: '', link: '' },
        {
          text: 'NodeJS',
          children: [{ text: 'Vue', link: '/nodejs/vue/' }],
        },
      ])
    })
  })
  describe('testNavToRelLinks', () => {
    it('link should be correct', () => {
      expect(links).toStrictEqual(['/nodejs/vue/'])
    })
  })
  describe('testCreateSideBarConfig', () => {
    it('sidebar should be correct', () => {
      expect(data).toStrictEqual({
        '/nodejs/vue/': [
          '/nodejs/vue/README.md',
          {
            text: 'Vuepress',
            children: ['/nodejs/vue/vuepress/Introduction.md', '/nodejs/vue/vuepress/README.md'],
          },
        ],
      })
    })
  })
})
