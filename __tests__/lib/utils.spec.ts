import * as Utils from "src/lib/utils"

describe("lib/utils", () => {
  it('testCapitalize', ()=> {
    expect(Utils.capitalize('aBc Def gHI')).toBe('ABc Def gHI')
  })
  it('testToTitle', ()=> {
    expect(Utils.toTitle('aBc Def gHI')).toBe('ABc Def GHI')
  })
})
