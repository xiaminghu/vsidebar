type Callable = () => void
type TwoWayCallable = () => Boolean

function all(array: any[]) {
  let flag = true
  for (let each of array) {
    flag = flag && each
  }
  return flag
}

export class TwoWayModel {
  _optionA?: TwoWayCallable
  _optionB?: TwoWayCallable
  _allCoorect?: Callable
  _aCorrect?: Callable
  _bCorrect?: Callable
  _allNot?: Callable

  optionA(func: TwoWayCallable) {
    this._optionA = func
    return this
  }
  optionB(func: TwoWayCallable) {
    this._optionB = func
    return this
  }
  allCorrect(func: Callable) {
    this._allCoorect = func
    return this
  }
  aCorrect(func: Callable) {
    this._aCorrect = func
    return this
  }
  bCorrect(func: Callable) {
    this._bCorrect = func
    return this
  }
  allNot(func: Callable) {
    this._allNot = func
    return this
  }
  run() {
    if (
      !all([
        this._optionA,
        this._optionB,
        this._allCoorect,
        this._aCorrect,
        this._bCorrect,
        this.allNot,
      ])
    ) {
      throw new Error('TwoWayModel: model params not all filled')
    }
    let aResult = (this._optionA as TwoWayCallable)()
    let bResult = (this._optionB as TwoWayCallable)()
    if (aResult && bResult) {
      ;(this._allCoorect as Callable)()
    } else if (aResult) {
      ;(this._aCorrect as Callable)()
    } else if (bResult) {
      ;(this._bCorrect as Callable)()
    } else {
      ;(this._allNot as Callable)()
    }
  }
}
