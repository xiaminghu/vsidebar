/**
 * 首字母大写
 * @param str 字符串
 * @returns 首字母大写的字符串
 */
export function capitalize(str: string) {
  return str ? str.substring(0, 1).toUpperCase() + str.substring(1) : str
}
/**
 * 将字符串转为标题格式
 * @param str 字符串
 * @returns 标题化的字符串
 */
export function toTitle(str: string) {
  return str.split(' ').map(capitalize).join(' ')
}
