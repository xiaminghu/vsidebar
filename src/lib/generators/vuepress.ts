import type { NavbarGroup, NavbarConfig, NavLink } from '../../types/vuepress'
import { AbstractGenerator } from './abstractGenerator'

export class VuepressGenerator extends AbstractGenerator {
  /**
   * 将 NavItem 按照以下规则转为 RelPath
   * - navItem 可能是 string，对于 string，直接跳过即可
   * - 如果 navItem.items，遍历 items 递归调用该函数
   * - 否则如果含有 navItem.link，验证通过则添加到 navItem.items
   * @param nav 导航栏配置
   * @returns string[]
   */
  // https://github.com/vuepress/vuepress-next/blob/main/packages/@vuepress/theme-default/src/client/components/NavbarLinks.vue#L127
  navToRelLinks(nav: NavbarConfig): string[] {
    let navLinks = [] as string[]
    for (let navItem of nav) {
      if (typeof navItem === 'string') {
        continue
      }
      if ((navItem as NavbarGroup).children) {
        navLinks = [...navLinks, ...this.navToRelLinks((navItem as NavbarGroup).children)]
      }
      // use else if, skip if navItem has children
      else if ((navItem as NavLink).link) {
        if (this.isValidLink((navItem as NavLink).link)) {
          navLinks.push((navItem as NavLink).link)
        }
      }
    }
    return navLinks
  }

  /**
   * - 创建空的 SideBarLink
   * - 设置 text 为【去除后缀、Capitalized】的文件名
   * - 设置 link 为【去除后缀】的文件路径
   * @param relPath
   * @returns
   */
  createSideBarLinkForFile(relPath: string): string {
    return this.processLink(relPath);
  }

  defaultIndexName() {
    return 'README'
  }
  processLink(link: string) {
    return link
  }
}
