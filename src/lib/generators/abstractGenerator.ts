import type { Config } from '../../types/index'
import type {
  MultiSideBarConfig,
  SideBarItem,
} from '../../types/vitepress'
import * as fs from 'fs'
import * as path from 'path'
import { capitalize } from '../utils'

export abstract class AbstractGenerator {
  config: Config

  abstract navToRelLinks(nav: any): string[]
  abstract createSideBarLinkForFile(relPath: string): any
  abstract defaultIndexName(): string
  abstract processLink(link: string): string

  constructor(config: Config) {
    this.config = config
  }

  run() {
    try {
      let data = {} as MultiSideBarConfig
      // may throw when user set a non-existing relpath
      let links = this.navToRelLinks(this.loadNavBar());
      // noexcept
      data = this.createSideBarConfig(links)
      // noexcept
      this.writeSiteBarData(data)
    } catch (e) {
      // show short error message
      console.error((e as Error).message)
    }
  }

  loadNavBar() {
    let abspath = path.resolve(this.config.navbar)
    // WARNING: will throw error when navbar doesn't exists or fail to load
    let navbar;
    try {
      navbar = require(abspath)
    } catch {
      // re-throw error by the way we want
      throw new Error(`fail to load navbar data: ${abspath}`)
    }
    return navbar
  }

  /**
   * 如果 link 符合以下要求，则返回 true
   * - 非 URL
   * - 以 / 结尾
   * @param link 链接
   * @returns
   */
  isValidLink(link: string): Boolean {
    try {
      new URL(link)
      return false;
    } catch {
      return link.endsWith('/') && fs.existsSync(path.join(this.config.docsDir, link));
    }
  }

  /**
   * 创建空的 SideBarGroup
   * @returns SideBarGroup
   */
  createEmptySideBarGroup() {
    return {
      text: '',
      // collapsable: true,  // not supported in vuepress@next
      children: [],
    }
  }

  /**
   * 按照如下规则为每一个 navLinks 创建对应的 sidebar
   * - 如果 relPath 对应的绝对路径不是文件夹，不创建 sidebar
   * - 如果是文件夹，调用 createSideBar 创建 SideBar
   *   如果返回值不是 undefined 且含有 children
   *   将 relPath 作为 key, children 作为 value 绑定给 data
   * @param relpath 相对路径，必定是一个
   * @returns MultiSideBarConfig
   */
  createSideBarConfig(relPaths: string[]): MultiSideBarConfig {
    let data = {} as MultiSideBarConfig
    for (let relPath of relPaths) {
      let absPath = path.join(this.config.docsDir, relPath)
      let stat = fs.statSync(absPath)
      // 非文件夹不创建 sidebar
      if (!stat.isDirectory) {
        continue
      }
      let result = this.createSideBar(relPath)
      if (result !== undefined && 'children' in result) {
        data[relPath] = result.children
      }
    }
    return data
  }

  /**
   * - 判断传入的路径对应的文件类型，只处理【文件夹】及【文件】
   *   - 如果是文件，设置 data 为 createSideBarLinkForFile(relPath)
   *   - 如果是文件夹
   *     - 创建空的 SideBarGroup
   *     - 设置 text 为【去除后缀、Capitalized】的文件名
   *     - 遍历文件夹内的【文件、文件夹】
   *       - 调用 createSideBar 递归创建
   *       - 如果返回值不为 undefined，添加返回值到 children
   *       - 如果 children 为空，不返回 data，返回 undefined
   * @param relPath 相对路径
   * @returns SideBarItem | undefined
   */
  createSideBar(relPath: string): SideBarItem | undefined {
    let absPath = path.join(this.config.docsDir, relPath)
    let stat = fs.statSync(absPath)
    let data: SideBarItem | undefined = undefined
    if (stat.isFile()) {
      data = this.createSideBarLinkForFile(relPath)
    } else if (stat.isDirectory()) {
      data = this.createEmptySideBarGroup()
      data.text = capitalize(this.processSuffix(path.basename(absPath)))
      let files = fs.readdirSync(absPath)
      for (let file of files) {
        // path.relative returns relative path which doesn't have forward slash at the begining
        // however vuepress requires it, so we add it automatically
        let newRelPath = '/' + path.relative(this.config.docsDir, path.join(absPath, file))
        let child = this.createSideBar(newRelPath)
        // 如果不是 directory 或 file，返回 undefined
        // undefined 的情况不添加到 children 中
        if (child !== undefined) {
          data.children.push(child)
        }
      }
      // 如果 children 为空，返回 undefined
      if (data.children.length == 0) {
        return undefined
      }
    }
    return data
  }

  processSuffix(path: string): string {
    return path.replace('.md', '')
  }

  writeSiteBarData(data: MultiSideBarConfig) {
    fs.writeFileSync(this.config.output, JSON.stringify(data))
  }
}
