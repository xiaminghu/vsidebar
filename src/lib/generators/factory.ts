import { Config } from "../../types";
import { AbstractGenerator } from "./abstractGenerator";
import { VitepressGenerator } from "./vitepress";
import { VuepressGenerator } from "./vuepress";

export class GeneratorFactory {
  static createGenerator(config: Config)
    : AbstractGenerator {
      switch(config.generator) {
        case 'vuepress': {
          return new VuepressGenerator(config)
        }
        case 'vitepress': {
          return new VitepressGenerator(config)
        }
      }
  }
}
