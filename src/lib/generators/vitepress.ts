import type {
  NavItem,
  NavItemWithChildren,
  NavItemWithLink,
  SideBarLink,
} from '../../types/vitepress'
import { AbstractGenerator } from './abstractGenerator'
import { capitalize } from '../utils'
import * as path from 'path'

export class VitepressGenerator extends AbstractGenerator {
  /**
   * 将 NavItem 按照以下规则转为 RelPath
   * - 如果 navItem.items，遍历 items 递归调用该函数
   * - 否则如果含有 navItem.link，验证通过则添加到 navItem.items
   * @param nav NavItem[]
   * @returns string[]
   */
  navToRelLinks(nav: NavItem[]): string[] {
    let navLinks = [] as string[]
    for (let navItem of nav) {
      if ((navItem as NavItemWithChildren).items) {
        navLinks = [...navLinks, ...this.navToRelLinks((navItem as NavItemWithChildren).items)]
      }
      // use else if, skip if navItem has children
      else if ((navItem as NavItemWithLink).link) {
        if (this.isValidLink((navItem as NavItemWithLink).link)) {
          navLinks.push((navItem as NavItemWithLink).link)
        }
      }
    }
    return navLinks
  }

  /**
   * - 创建空的 SideBarLink
   * - 设置 text 为【去除后缀、Capitalized】的文件名
   * - 设置 link 为【去除后缀】的文件路径
   * @param relPath
   * @returns
   */
  createSideBarLinkForFile(relPath: string): SideBarLink {
    let data = {
      text: capitalize(this.processSuffix(path.basename(relPath))),
      link: this.processLink(relPath),
    }
    return data
  }

  defaultIndexName() {
    return 'index'
  }
  processLink(link: string) {
    return this.processSuffix(link).replace(this.defaultIndexName(), '')
  }
}
