#!/usr/bin/env node
import { Config, GeneratorFactory } from '.'
import { TwoWayModel } from './model'
import { Command, Option } from 'commander'
import * as fs from 'fs'
import * as path from 'path'

const VERSION = process.env.npm_package_version || '0.0.1'
const program = new Command()
program.version(VERSION)

const HELP_TEXT_BEFORE_ALL = `
vsidebar is a tool helps you generator sidebar configuration
for vuepress-next and vitepress automatically. Before use,
please read the instructions below:

Conventions:
  $projectDir:  root of your project
  $docsDir:     directory of your documentations
  $configDir:   $docsDir/.vuepress or $docsDir/.vitepress

Notes:
  1. use vsidebar in $projectDir
  2. none of the options are required, vsidebar detect it automatically
  3. use -d when your docs are not located in $projectDir/docs
  4. use -g when your have .vuepress and .vitepress simultaneously in $docsDir
  5. use -i when your navbar.json or nav.json is not located in $configDir
  6. use -o when you don't want sidebar.json generated in $configDir/sidebar.json
`

program
  .option('-d, --docs-dir <type>', 'location of <docs-dir>', 'docs')
  .addHelpText('beforeAll', HELP_TEXT_BEFORE_ALL)
  .option('-o, --output <type>', 'output file (sidebar), relative to <docs-dir>', 'sidebar.json')
  // detect nav.json or navbar.json if not provided
  .option('-i, --input <type>', 'input file (navbar), relative to <docs-dir>')
  // actually no need to specify this, since we colud detect it automatically
  // but maybe someone want to try vitepress and vuepress simultaneously, we offer this
  .addOption(
    new Option('-g, --generator <type>', 'generator type, auto detect if not provided').choices([
      'vuepress',
      'vitepress',
    ])
  )
  .parse(process.argv)

const options = program.opts()

try {
  /* Determine Docs Directory */
  let docsDir = path.join(process.cwd(), options.docsDir || 'docs')
  let configDir = ''
  if (!fs.existsSync(docsDir)) {
    throw new Error(`docs directory doesn't exist: ${docsDir}`)
  }
  let stat = fs.statSync(docsDir)
  if (!stat.isDirectory()) {
    throw new Error(`${docsDir} is not a directory`)
  }

  /* Determine Config Directory */
  let generator: 'vuepress' | 'vitepress' = 'vuepress'
  let vuepressConfigDir = path.join(docsDir, '.vuepress')
  let vitepressConfigDir = path.join(docsDir, '.vitepress')
  new TwoWayModel()
    .optionA(() => fs.existsSync(vuepressConfigDir))
    .optionB(() => fs.existsSync(vitepressConfigDir))
    .allCorrect(() => {
      if (!options.generator) {
        throw new Error(
          '$docsDir/.vuepress and $docsDir/.vitepress exists simultaneously\n' +
            'please use -g option to specify generator'
        )
      } else {
        configDir = path.join(docsDir, `options.generator`)
      }
    })
    .aCorrect(() => {
      generator = 'vuepress'
      configDir = vuepressConfigDir
    })
    .bCorrect(() => {
      generator = 'vitepress'
      configDir = vitepressConfigDir
    })
    .allNot(() => {
      throw new Error('Cannot detect config directory $docsDir/.vuepress or $docsDir/.vitepress')
    })
    .run()

  /* Determine NavBar */
  let navbar = ''
  let navJson = path.join(configDir, 'nav.json')
  let navBarJson = path.join(configDir, 'navbar.json')

  new TwoWayModel()
    .optionA(() => fs.existsSync(navJson))
    .optionB(() => fs.existsSync(navBarJson))
    .allCorrect(() => {
      throw new Error(
        '$configDir/nav.json and $configDir/navbar.json exists simultaneously\n' +
          'please use -i option to specify input file'
      )
    })
    .aCorrect(() => {
      navbar = navJson
    })
    .bCorrect(() => {
      navbar = navBarJson
    })
    .allNot(() => {
      throw new Error('Cannot detect input file $configDir/nav.json and $configDir/navbar.json')
    })
    .run()

  /* Determine SideBar */
  let output = path.join(configDir, options.output || 'sidebar.json')
  let parentDir = path.dirname(output)
  if (!fs.existsSync(parentDir)) {
    throw new Error(`directory ${parentDir} doesn't exists, fail to write output`)
  }

  /* Create Config */
  let config: Config = {
    generator: generator,
    docsDir: docsDir,
    navbar: navbar,
    output: output,
  }
  /* Run */
  GeneratorFactory.createGenerator(config).run()
} catch (e) {
  console.error((e as Error).message)
}
