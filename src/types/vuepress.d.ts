export type {
  NavLink,
  NavbarConfig,
  NavbarGroup,
  NavbarItem,
  SidebarConfig,
  SidebarConfigObject,
  SidebarConfigArray,
  SidebarItem
} from '@vuepress/theme-default'
