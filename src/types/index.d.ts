export interface Config {
  generator: 'vuepress' | 'vitepress',
  navbar: string,
  docsDir: string,
  output: string,
}
